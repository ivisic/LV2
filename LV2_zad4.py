
import numpy as np
import matplotlib.pyplot as plt
br=1000
x=30
i=0
sv_vector=np.zeros(br)
dev_vector=np.zeros(br)
while(br!=0):    
    a=np.random.choice([1,2,3,4,5,6], size=(x,)) 
    
    sv=sum(a)/x         
    a_dev=np.std(a)     
    dev_vector[i]=a_dev
    sv_vector[i]=sv

    br=br-1
    i=i+1

plt.hist(dev_vector) 
plt.hist(sv_vector)  
plt.show()